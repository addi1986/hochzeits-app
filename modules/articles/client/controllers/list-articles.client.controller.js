(function() {
  'use strict';

  angular
    .module('articles')
    .controller('ArticlesListController', ArticlesListController);

  ArticlesListController.$inject = ['ArticlesService', '$scope', 'Socket', '$interval'];

  function ArticlesListController(ArticlesService, $scope, Socket, $interval) {
    var vm = this;

    init();

    function init() {
      generateImages();
      // If user is not signed in then redirect back home

      // Make sure the Socket is connected
      if (!Socket.socket) {
        Socket.connect();
      }

      // Add an event listener to the 'chatMessage' event
      Socket.on('chatMessage', function(message) {
        // console.info("hjklööjhj", message)
      });

      // Remove the event listener when the controller instance is destroyed
      $scope.$on('$destroy', function() {
        Socket.removeListener('chatMessage');
      });
    }


    $scope.emitTest = function() {
      // Create a new message object
      var message = {
        text: "ssssss"
      };

      // Emit a 'chatMessage' message event
      Socket.emit('chatMessage', message);
    }


    $scope.randomList = function(list) {
      var firstThree = list.slice(0, 3);
      list.splice(0, 3)
      var sortedList = list.sort(function() {
        return 0.5 - Math.random();
      });

      return firstThree.concat(sortedList)
    }

    $interval(function() {
      generateImages();
      // console.info("interval", vm.articles);
    }, 60000);

    function generateImages() {
      ArticlesService.query().$promise.then(function(result) {
        vm.articles = $scope.randomList(result);
      })
    }
    vm.myInterval = 8000;

    var $item = angular.element('.carousel .item');
    var $wHeight = angular.element(window).height();
    $item.eq(0).addClass('active');
    $item.height($wHeight);
    $item.addClass('full-screen');

    angular.element('.carousel img').each(function() {
      angular.element(this).parent().css({
        'background-color': 'black'
      });
      angular.element(this).remove();
    });

    angular.element(window).on('resize', function() {
      $wHeight = angular.element(window).height();
      $item.height($wHeight);
    });

  }
}());
