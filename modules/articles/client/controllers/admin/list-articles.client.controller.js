﻿
(function() {
  'use strict';

  angular
    .module('articles.admin')
    .controller('ArticlesAdminListController', ArticlesAdminListController);

  ArticlesAdminListController.$inject = ['ArticlesService', '$scope', '$state', '$window', 'Notification'];

  function ArticlesAdminListController(ArticlesService, $scope, $state, $window, Notification) {
    var vm = this;

    vm.articles = ArticlesService.query();

    $scope.deleteImage = function(article) {
      if ($window.confirm('Are you sure you want to delete?')) {
        article.$remove(function() {
          $state.go('articles.list');
          Notification.success({
            message: '<i class="glyphicon glyphicon-ok"></i> Article deleted successfully!'
          });
          vm.articles = ArticlesService.query();
        });
      }
    };
  }
}());
