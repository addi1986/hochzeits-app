﻿
(function() {
  'use strict';
  angular
    .module('articles.admin')
    .controller('ArticlesAdminController', ArticlesAdminController);

  ArticlesAdminController.$inject = ['$scope', '$state', '$window', 'articleResolve', 'Authentication', 'Notification', 'Upload', '$location', '$timeout', 'Socket'];

  function ArticlesAdminController($scope, $state, $window, article, Authentication, Notification, Upload, $location, $timeout, Socket) {
    var vm = $scope;

    vm.article = article;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.timeout = timeout;
    vm.generateThumb = generateThumb;
    vm.uploadPic = uploadPic;

    init();

    function init() {
      // If user is not signed in then redirect back home
      // console.info('init u')
      // Make sure the Socket is connected
      if (!Socket.socket) {
        // console.info('connect u')
        Socket.connect();
      }
    }


    // Remove existing Article
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.article.$remove(function() {
          $state.go('admin.articles.list');
          Notification.success({
            message: '<i class="glyphicon glyphicon-ok"></i> Article deleted successfully!'
          });
        });
      }
    }

    // Save Article
    function save(picFile) {
      // Create a new article, or update the current instance
      vm.article.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.articles.list'); // should we send the User to the list or the updated Article's view?
        Notification.success({
          message: '<i class="glyphicon glyphicon-ok"></i> Article saved successfully!'
        });
      }

      function errorCallback(res) {
        Notification.error({
          message: res.data.message,
          title: '<i class="glyphicon glyphicon-remove"></i> Article save error!'
        });
      }
    }

    function timeout(file) {
      // console.log('do timeout');
      $timeout(function() {
        var fileReader = new FileReader();
        fileReader.readAsDataURL(file);
        // console.log('read');
        fileReader.onload = function(e) {
          $timeout(function() {
            file.dataUrl = e.target.result;
            // console.log('set url');
          });
        };
      });
    }

    function generateThumb(file) {
      if (file) {
        if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
          timeout(file);
        }
      }
    }

    function uploadPic(picFile) {

      picFile.upload = Upload.upload({
        url: '/articleupload',
        data: {
          file: picFile
        }
      });
      picFile.upload.then(function(response) {
        // console.info("beforeemit", Socket.socket)

        var message = {
          text: "uploaded image"
        };

        // Emit a 'chatMessage' message event
        Socket.emit('chatMessage', message);
        $state.go('home'); // should we send the User to the list or the updated Article's view?
        Notification.success({
          message: '<i class="glyphicon glyphicon-ok"></i> Bild erfolgreich hochgeladen!'
        });
      }, function(response) {
        if (response.status > 0) {
          Notification.error({
            message: response.data.message,
            title: '<i class="glyphicon glyphicon-remove"></i> Es ist leider ein Fehler beim hochladen aufgetreten!'
          });
        }
      }, function(evt) {
        // Math.min is to fix IE which reports 200% sometimes
        picFile.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
      });

    }
  }
}());
